import machine
from ssd1306 import SSD1306_I2C
from writer import Writer
import freesans20

def print_oled(text):
    WIDTH = const(128)
    HEIGHT = const(64)
    i2c = machine.I2C(scl=machine.Pin(4), sda=machine.Pin(5))
    ssd = SSD1306_I2C(WIDTH, HEIGHT, i2c)
    wri = Writer(ssd, freesans20, verbose=False)
    Writer.set_clip(True, True)
    Writer.set_textpos(0, 0)
    wri.printstring(text)
    ssd.show()
