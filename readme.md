## README
Once its start micropython will run boot.py and then main.py

'upload' folder contain all files that on Esp32
## OLED
```python
from oled import print_oled
print_oled("Hello\nWorld!")
```

## Blink LED
```python
import machine,time
led = machine.Pin(12,machine.Pin.OUT)
while True:
    led.value(1)
    time.sleep(0.5)
    led.value(0)
    time.sleep(0.5)
```

## .zprofile
```bash
alias ampy_esp32='ampy -p /dev/tty.SLAB_USBtoUART'
```

## Upload boot.py
```
ampy_esp32 put boot.py
```

## List file in micropython
```
ampy_esp32 ls
```

## Run boot.py
```
ampy_esp32 run boot.py
```

## Get boot.py from board
```
ampy_esp32 get boot.py
```

## Credit
```http
https://github.com/peterhinch
https://github.com/peterhinch/micropython-samples
https://github.com/peterhinch/micropython-font-to-py
```